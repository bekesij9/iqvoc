# Fuseki in a Docker container

For better maintainability we let run fuseki in a docker container, so
there is no installation hassle.

The `docker-compose.yml` has a restart command, so it runs forever.

New triplestore data are in `/var/www/fuseki/dckr`, old ones are preserved in
`/var/www/fuseki` (seems to be a quite complicated setup for `skosmos`).

Environment variables are in `.env`, which is not subject to revision control.

## ToDo:

* How can we switch to user `fuseki` running the docker container 
  (`docker-compose` complains about not having found this user in 
  `password`... its there, though)?

* Backup of sorts