# PHDR iQvoc

Forked in 2019-02 from https://github.com/innoq/iqvoc

There are some smallish changes to the codebase, mainly layout issues and 
customizations.

## Features Added:

 * Fuseki adapter for triplestore sync

 * Sync adapter option (in Configuration)

 * `https` triplestore urls possible


## Image preparation (for development)

Use `innoq/iqvoc_postgres` image for preparation; use `production` ENV in
the `docker-compose` file, or
else it won't work (later you can switch to `development` and `sqlite`). 
Remember this is the source code *inside* the container with no volumes mounted.

```
docker cp Gemfile ivq_web_1/iqvoc
(insert gem 'base32-crockford-checksum')
docker cp Gemfile ivq_web_1/iqvoc
```
then `docker exec -ti iqv_web_1 bash` and `bundle install` so that
32-crockford checksum gem is installed

`docker commit iqv_web_1 bekesij9/iqv_phdr:latest` to create an image for `docker-compose`

## Deployment

Also added a restart script for rails-server container for development; for 
production just use `passenger-config restart-app /`.

IqVoc is started by Apache (via Phusion Passenger: as in dev container)

Phusion serves IqVoc from subdirectory (`/vocabulary`), so that we can 
map fuseki to the path `fuseki` (invoked by `docker-compose` and `restart`).

**ATTN:** 

   * in `/etc/apache2/envvars` 
     I had to set `export SECRET_KEY_BASE=xxxxxxxx...` to be submitted to production Rails)

   * IqVoc cannot provide for correct asset links (fonts and icons), so
     I symlinked `public/assets` to `assets` in the root of the apache virtual host.

   * Jobs worker deployment: `export RAILS_ENV=production;bundle exec rake jobs:work`, so the
     correct environment setting is used


## ToDo:

* Convey a SPARQL query for looking up stuff in graphs -- OK, done (cf. 
  `sparql_examples.md`)

```
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
select ?concept ?g ?label
where {
  graph ?g {
    ?concept skos:broader ?up .
    ?up skos:prefLabel "Obst"@de .
    ?concept skos:prefLabel ?label .
}
}
```

## Re-Sync Triplestore

So we can remove botched triples and test entry relics.

For MySQL, use:

```
update concepts set rdf_updated_at = NULL where rdf_updated_at IS NOT NULL;
```

And then do a triplestore sync in the admin UI

## Sync Triplestore Revisited:

Using a bash script:

```
#!/bin/sh
# syncing iqvoc to triplestore
#
cd /var/www/iqvoc.phdr.gitlab
echo `date -Iminutes`
export RAILS_ENV=production
#pwd
#echo $RAILS_ENV
# bundle exec rake sync:all[http://vocab.phaidra.org/vocabulary/] --trace
/usr/bin/curl --user admin@iqvoc:xAUuFgAAv4tB -X POST -F utf8="&#y2713" -s -L \
        -F authenticity_token=RMV6GKAgtKSCpLSbXjMTZgac2decXIv73qRiTuwCieYka15VMf+nY0gTbpiCLyL95S8lHB46b+ObpOOaKg== \
        https://vocab.phaidra.org/vocabulary/en/triplestore_sync.html
```

Or, after this works quite well deployed by crontab, we can switch on 
`Automatic triplestore synchronization` in the UI (Configuration). This does a
sync immediately after change or update of concepts.


### 2022-12-16

Syncing works well, but there is an edge case:

* If we add a Concept to a Collection, this is reflected in iQvoc, but does not trigger
  a triplestore sync (perhaps not a change in the concept itself, only in the collection)

* workaround would be to create a new version for the concept; but this is clumsy at best


## Operations

To switch on `debug`, change value in `config/environments/production.rb` from
`config.log_level = :info` to
`config.log_level = :debug`, perhaps afterwards `apachectl graceful`.






See [original documentation](/../wikis/home)