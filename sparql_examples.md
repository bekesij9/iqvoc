# SPARQL example queries for PHAIDRA

**ATTN:** Collections are not synchronized to triplestore, but membership in 
          collection is, so that can be used

## Select ALL concepts from collection (ids and labels)

Collection: https://vocab.phaidra.org/vocabulary/en/collections/W6R8-EAQS.html 

 * ID must be hardcoded
 
 * expired concepts are retrieved as well! 

```
PREFIX v: <https://vocab.phaidra.org/vocabulary/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?id ?label
WHERE {
  graph ?g {
    ?id :memberOf  v:W6R8-EAQS .
    ?id skos:prefLabel ?label .  
  }
}
```

## Select non-expired concepts from collection (ids and labels)

        <https://vocab.phaidra.org/vocabulary/schema#expires>
                "2019-04-02" ;


```
PREFIX : <https://vocab.phaidra.org/vocabulary/schema#>
PREFIX v: <https://vocab.phaidra.org/vocabulary/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?id ?label ?exp
WHERE {
  graph ?g {
  ?id :memberOf  v:VPSR-KHRC .
  ?id skos:prefLabel ?label .  
    OPTIONAL {?id :expires ?exp .
       }
  }
}
```
returns this

| subject | label | exp |
| ------- | ----- | --- |
| https://vocab.phaidra.org/vocabulary/CRGV-097N | black marble | |
| https://vocab.phaidra.org/vocabulary/EXRJ-GCYG | shampoo | 2019-04-02 |
| https://vocab.phaidra.org/vocabulary/EXRJ-GCYG | Shampoo | 2019-04-02 |

With former select one could check expiry dates on client side (not elegant)

But now:

 * Select all those without "expires"

 * Select all those with "expires" and filter

 * Join both


On client side replace `VPSR-KHRC` with collection id and `2019-04-03` with 
present date or present date minus one
(watch out for the one-off problem: if it expires today, will it be still
shown today?):

```
PREFIX : <https://vocab.phaidra.org/vocabulary/schema#>
PREFIX v: <https://vocab.phaidra.org/vocabulary/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>


SELECT DISTINCT  ?id ?label ?exp
WHERE {
   {graph ?g {
    ?id :memberOf  v:VPSR-KHRC .
    ?id skos:prefLabel ?label .  
     ?id :expires ?exp . 
    } FILTER (?exp > "2019-04-03" )
    }
  UNION {
    graph ?g {
     ?id :memberOf  v:VPSR-KHRC .
    ?id skos:prefLabel ?label .  
     NOT EXISTS { ?id :expires ?exp . }
  }
  }
} 
```

... and it works fine