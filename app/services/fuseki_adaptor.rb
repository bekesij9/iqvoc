require "net/https"
require "cgi"

require_relative "iq_triplestorage"

class IqTriplestorage2::FusekiAdaptor < IqTriplestorage2::BaseAdaptor2

  def initialize(host, options={})
    super
    # validate to avoid nasty errors
    raise(ArgumentError, "username must not be nil") if @username.nil?
    @repo = options[:repository]
    raise(ArgumentError, "repository must not be nil") if @repo.nil? 
    Rails.logger.debug("RDF-Sync Fuseki Adaptor initialized with #{@host}")   
  end

  def reset_(uri)
    return sparql_pull("CLEAR GRAPH <#{uri}>") # XXX: s/CLEAR/DROP/ was rejected (405)
  end

  # expects a hash of N-Triples by graph URI (from SESAME)
  def batch_update(triples_by_graph)
    # path = "/#{CGI.escape(@repo)}"
    # why should we use CGI.escape? I do not (yet) see the use case (JB)
    path = @repo
    path = URI.join("#{@host}", path[1..-1]).path
    # host part is provided by class
    path = "#{path}/update" # we use sparql update 1.1:
    #del_params = triples_by_graph.keys.
    #    map { |graph| val = CGI.escape("<#{graph}>"); "context=#{val}" }.
    #    join("&")
    graph_list = []
    data = triples_by_graph.map do |graph_uri, ntriples|
      # triples with graph
      # <#{graph_uri}> "\n#{ntriples\n}\n"
      newtr = rewrite_blank_nodes(ntriples)
      graph_list << graph_uri
      Rails.logger.debug("RDF-Sync: #{path} .. new ntriples: #{newtr}")
      Rails.logger.debug("   for graph_uri: #{graph_uri}")
      "INSERT DATA  { GRAPH <#{graph_uri}> {\n#{newtr}\n} };\n"
    end.join("\n\n")
    data.sub!('?format=html', '') # don't know where that format=html comes from, but it has to go
    data_delete = graph_list.map do |graph_uri|
      "CLEAR SILENT GRAPH <#{graph_uri}>; \n"
    end.join("\n")
    res = http_request("POST", path, data_delete,
          { "Content-Type" => "application/sparql-update" })
    Rails.logger.debug("RDF-Sync: delete: #{data_delete}\n\t#{res.code}, #{res.message}")
    res.each {|key, val| Rails.logger.debug("  #{key}:   #{val}") } # %-14s = %-40.40s\n", key, val }
    return false unless res.code == "204"
    res = http_request("POST", path, data,
          { "Content-Type" => "application/sparql-update" })
    Rails.logger.debug("RDF-Sync: res.code #{res.code}")
    Rails.logger.info("Synced #{triples_by_graph.length} concepts with fuseki.")
    return res.code == "204"
  end

  def rewrite_blank_nodes(ntriples)
    # rewrites blank nodes to [] variant 
    # only ttl output works this way, nt doesn't
    output = []
    blanks = []
    ntriples.gsub!(/http\:\/\/localhost\:3000\//, Iqvoc.config['site_url'])
    # Rails.logger.debug("RDF-Sync ntriples: #{ntriples}\n")
    ntriples.split("\n").each do |l| 
      if l.index("_:")
        blanks << l
      else
        output << l
      end
    end
    # we have to use the first occurence of "(_:\w*)" as key and the rest inside
    # like this [ bnode1 ; bnode2 ]
    # and add it joint to output
    # fuseki transforms this into _:xx form again...
    # search for bnode ids via regex
    replacements = ntriples.scan(/(_:\w*) \.$/)
    replacements = replacements.map { |x| x[0] }
    # Rails.logger.debug("RDF-Sync replacements: #{replacements}")
    bnode_rep = blanks[0] # the first one carries the id
    # replacement = bnode_rep.scan(/_:\w*/)[0]
    bnodes = {}
    bnodes_prefix = {}
    replacements.each do |repl|
      bnodes[repl] = []
      blanks.each do |l|
        if l.index("#{repl} ")  # or else _:b1 AND _:b11 are found
          if l.index(repl) > 15 #  "_:nn" at end of line
            bnodes_prefix[repl] = l # .sub(/#{repl}/o, "")
            # blanks.delete(l)
          else
            stub =  l.sub(/#{repl}/, "")
            stub.sub!(/\.$/, "")
            bnodes[repl] << stub
            # blanks.delete(l)
          end
        end
      end
    end
    bnode_nodes = bnodes.map do |repl, val| 
      nodes = val.join(";\n\t")
      nodes.sub!(/#{repl}/, "")
      nodes.sub!(/_\./, "")
      npref = bnodes_prefix[repl]
      npref.sub!(/#{repl} \./, "")
      "#{npref} [ #{nodes} ] ."
      end.join("\n")
    output << bnode_nodes # bnode_rep.sub!(/#{replacement}/, "[ #{bnode_node} ]")
    return output.join("\n")
  end


  # expects a hash of N-Triples by graph URI
  def batch_update_virtuoso(triples_by_graph)
    # apparently Virtuoso gets confused when mixing CLEAR and INSERT queries,
    # so we have to do use separate requests

    reset_queries = triples_by_graph.keys.map do |graph_uri|
      "CLEAR GRAPH <#{graph_uri}>" # XXX: duplicates `reset`
    end
    success = sparql_query(reset_queries)
    return false unless success

    insert_queries = triples_by_graph.map do |graph_uri, ntriples|
      "INSERT IN GRAPH <#{graph_uri}> {\n#{ntriples}\n}"
    end
    success = sparql_query(insert_queries)

    return success
  end


  # uses push method if `rdf_data` is provided, pull otherwise
  def update_(uri, rdf_data=nil, content_type=nil)
    reset(uri)

    if rdf_data
      res = sparql_push(uri, rdf_data.strip, content_type)
    else
      res = sparql_pull(%{LOAD "#{uri}" INTO GRAPH <#{uri}>})
    end

    return res
  end

  def sparql_push_(uri, rdf_data, content_type)
    raise TypeError, "missing content type" unless content_type

    filename = uri.gsub(/[^0-9A-Za-z]/, "_") # XXX: too simplistic?
    path = "/DAV/home/#{@username}/rdf_sink/#{filename}"

    res = http_request("PUT", path, rdf_data, { # XXX: is PUT correct here?
      "Content-Type" => content_type
    })

    return res.code == "201"
  end

  def sparql_pull_(query)
    path = "/DAV/home/#{@username}/rdf_sink" # XXX: shouldn't this be /sparql?
    res = http_request("POST", path, query, {
      "Content-Type" => "application/sparql-query"
    })
    return res.code == "200" # XXX: always returns 409
  end

  # query is a string or an array of strings
  def sparql_query(query)
    query = query.join("\n\n") + "\n" rescue query
    path = "/DAV/home/#{@username}/query"

    res = http_request("POST", path, query, {
      "Content-Type" => "application/sparql-query"
    })

    return res.code == "201"
  end

end