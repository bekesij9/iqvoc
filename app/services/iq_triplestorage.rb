#
# Duplicated and refined this from iq_triplestorage module
# (https issues -- we leave the original on github, since
# it is only one file:)
#

require "net/https"

module IqTriplestorage2
  VERSION = "0.2.4"

  class BaseAdaptor2
    # we call it BaseAdaptor2 to hint at the https enhancement
    attr_reader :host

    def initialize(host, options={})
      @host = URI.parse(host).normalize
      @username = options[:username]
      @password = options[:password]
    end

    def http_request(method, path, body=nil, headers={})
      uri = URI.join("#{@host}/", path)
      if uri.port == 443
        Rails.logger.debug("we use https")
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      req = Net::HTTP.const_get(method.to_s.downcase.capitalize).new(uri.to_s)
      req.basic_auth(@username, @password) if (@username && @password)
      headers.each { |key, value| req[key] = value }
      req.body = body if body
      if uri.port == 443
        return http.request(req)
      else
        return Net::HTTP.new(uri.host, uri.port).request(req)
      end
    end

  end
end
